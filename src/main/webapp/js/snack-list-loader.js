// Fetch snacks and list them
function fetchSnacks() {
    const url = "./snack-list";
    fetch(url).then((response) => {
        return response.json();
    }).then((snacks) => {
        const snackList = document.getElementById("snack-list");
        snackList.innerHTML = '';
        snacks.forEach((snack) => {
            const snackDiv = buildSnackDiv(snack);
            snackList.appendChild(snackDiv);
        });
    });
//
//    var Inventory = Java.type("data.Inventory()");
//    var i = new Inventory();
//    var snacks = i.getSnacks();
//    const snackList = document.getElementById("snack-list");
//    snackList.innerHTML = '';
//    snacks.forEach((snack) => {
//        const snackDiv = buildSnackDiv(snack);
//        snackList.appendChild(snackDiv)
//    });
}

function buildSnackDiv(snack) {
    const snackNameDiv = document.createElement('div');
    snackNameDiv.appendChild(document.createTextNode(snack.name));
    return snackNameDiv;
}

function onLoad() {
    fetchSnacks();
}
