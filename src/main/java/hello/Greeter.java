package hello;

public class Greeter {
	public String sayHello() {
		return "Hello!";
	}

	public int sum(int init, int add) {
		return init + add;
	}
}
