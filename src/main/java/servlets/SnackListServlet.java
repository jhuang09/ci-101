package servlets;

import data.Inventory;
import data.Snacks;
import java.util.List;
import com.google.gson.Gson;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/gs-maven/snack-list")
public class SnackListServlet extends HttpServlet {
    private Inventory i;
    @Override
    public void init() {
        i = new Inventory();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        List<Snacks> snacks = i.getSnacks();
        Gson gson = new Gson();
        String json = gson.toJson(snacks);
        response.getWriter().println(json);
        }

}
