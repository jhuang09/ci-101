package data;

import java.util.List;
import java.util.ArrayList;

public class Inventory {
	private List<Snacks> allSnacks = new ArrayList<Snacks>();

	public Inventory() {
		addSnack(new Snacks());
		addSnack(new Snacks("Skittles"));
		addSnack(new Snacks("Cinnamon Toast Crunch"));
	}

	public List<Snacks> getSnacks() {
		return allSnacks;
	}

	public void addSnack(Snacks s) {
		allSnacks.add(s);
	}

	public void removeSnack(Snacks s) {
		allSnacks.remove(s);
	}

	public int getSize() {
		return allSnacks.size();
	}
}
