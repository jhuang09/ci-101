#!/bin/bash

cd src/test/java
TESTFILES=$(circleci tests glob "**/*.java" | circleci tests split)
TESTSARRAY=($TESTFILES)
ALLTESTS=""
for ((i = 0; i <${#TESTSARRAY[@]}; i++));
  do
    if [ $i -gt 0 ]; then
      ALLTESTS="${ALLTESTS},${TESTSARRAY[$i]}"
    else
      ALLTESTS="${TESTSARRAY[$i]}"
    fi
  done
cd ../../..
echo $ALLTESTS
echo "mvn test -Dtest=${ALLTESTS}"
mvn test -Dtest=${ALLTESTS}
