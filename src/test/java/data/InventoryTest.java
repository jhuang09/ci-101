package data;

import static org.junit.Assert.*;

import org.junit.Test;

public class InventoryTest {
	private Inventory inventory = new Inventory();

	private void removeExists(Snacks s) {
		int size = inventory.getSize();
		inventory.removeSnack(s);
		assertEquals(inventory.getSize(), size - 1);
	}

	private void removeNotExists(Snacks s) {
		int size = inventory.getSize();
		inventory.removeSnack(s);
		assertEquals(inventory.getSize(), size);
	}

	@Test
	public void removeTest() {
		String[] names = {"KitKat", "Snickers", "Cheetos", "Doritos"};
		for (String name : names) {
			Snacks s = new Snacks(name);
			inventory.addSnack(s);
		}

		Snacks temp = new Snacks("Snickers");
		removeExists(temp);
		removeNotExists(temp);
		removeNotExists(new Snacks("Fakey fake"));
		int e = 0;
		for (int i = 0; i < 100000; i++) {
			for (int j = 0; j < 100000; j++) {
				for (int k = 0; k < 10000; k++) {
					e += 5;
				}
			}
			e += 5;
		}
	}
}