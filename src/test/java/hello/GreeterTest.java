package hello;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import org.junit.Test;

public class GreeterTest {

	private Greeter greeter = new Greeter();
	
	private void sum(int start, int add, int expected) {
		int actual = greeter.sum(start, add);
		assertEquals(actual, expected);
	}

	@Test
	public void greeterSaysHello() {
		assertThat(greeter.sayHello(), containsString("Hello"));
		int e = 0;
		for (int i = 0; i < 10000; i++) {
			for (int j = 0; j < 10000; j++) {
				for (int k = 0; k < 100000; k++) {
					e += 5;
				}
			}
			e += 5;
		}
	}

	@Test
	public void greeterSum() {
		sum(1, 1, 2);
		sum(1, -1, 0);
		sum(-10023, -324, -10347);
		sum(1000, 20, 1020);
		int e = 0;
		for (int i = 0; i < 10000; i++) {
			for (int j = 0; j < 1000; j++) {
				for (int k = 0; k < 100000; k++) {
					e += 5;
				}
			}
			e += 5;
		}
	}

}
